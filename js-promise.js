/** 
1. This is the definition of main logic(first part)
2. resolve & reject are params of callback funcs
3. Normally, this logic are Broswer async API, like event listener, XHttpRequest, Ajax, based on the result state to call resolve or reject
**/
    let p = new Promise((resolve, reject) => {
    let a = 1 + 1;
    console.log('456');
    if (a == 2) {
        resolve('sucess data: resolve (pass in then)')
    } else {
        reject('fail data: reject (pass in catch)')
    }
})

/** 
1. This is the definition of callback funcs(secord part)
2. then func map to resolve, catch func map to reject
**/
p.then((msg) => {
    console.log('this is the then:' + msg)
}).catch((msg) => {
    console.log('this is the catch:' + msg)
})
console.log('123')
// log sequece is 456 -> 123 -> this is ...
// So the Promise callback will always put in event stack

// Promise.all + Promise.race + Promise.any