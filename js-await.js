/**
1. await and async are better way to write promise
2. await similar to then (Promise<data> - data), async similar to Promise (data - Promise<data>) 
3. error need to use try - catch structure
4. In react, we can build async hook with external state to record the api state, but we may not be able to use here, because the they are actually callbacks and because of the hidden closure rule, the external state we use here is not the latest
**/
async function doWork() {
    try {
        const response = await fetch('Facebook')
        console.log('Response Received')
        const processedResponse = await fetch(':' + response)
        console.log(processedResponse)
    } catch (error) {
        console.log(error)
    }
}