/** 1. first then the res has a body, but this body 
can not be accessed, unless you call the json func
and this json func will return a new promise with
a json data
**/
/** 2. fetch catch error only happen when the fetch function is failed. The response error not catched, so we need to use if(res.ok) check manually, if not ok, create a new error promise with reject(), this will be catched by next catch
**/
/** 3. fetch if POST request need to set up 1)method 2) headers -> Content-Type: application/json 3) body JSON.stringify(object data)
**/
fetch('https://reqres.in/api/users/23')
// This step will return a Promise<error/data>
    .then(res => {
        if(res.ok) {
            return res => res.json()
        } else {
            return Promise.reject('not exist')
        }
    })
    .then(data => console.log(data))
    .catch(err => console.log(err))