 // method 1
 const people = [1,2,3];
 const peopleLoop = async() => {
     for (const p of people) {
         const data = await fetch(`https://reqres.in/api/users/${p}`);
         console.log(data)
     }
 }
 peopleLoop();

 // method 2
 const people2 = [1,2,3];
 const peopleLoop2 = people2.map(async p => {
     const data = await fetch(`https://reqres.in/api/users/${p}`);
     console.log(data);
     return p;
 });

 /**
 m1 vs m2: 
 You can think m1 is await in order in a big async
 m2 is several async + await in map func
 m2 the map return a Promise, not the p again & the return p is the new peopleLoop[0].then 's data..
 So, m1 console is always in sequence, m2 console is not always in sequence (depend on api speed)
 This means m1 the await wait each other
 **/
